-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.63-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema manet
--

CREATE DATABASE IF NOT EXISTS manet;
USE manet;

--
-- Definition of table `pwd`
--

DROP TABLE IF EXISTS `pwd`;
CREATE TABLE `pwd` (
  `id` varchar(45) NOT NULL,
  `pwd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pwd`
--

/*!40000 ALTER TABLE `pwd` DISABLE KEYS */;
INSERT INTO `pwd` (`id`,`pwd`) VALUES 
 ('1','11111');
/*!40000 ALTER TABLE `pwd` ENABLE KEYS */;


--
-- Definition of table `routing`
--

DROP TABLE IF EXISTS `routing`;
CREATE TABLE `routing` (
  `id` int(10) unsigned NOT NULL DEFAULT '1',
  `router1` int(10) unsigned NOT NULL DEFAULT '1',
  `router2` int(10) unsigned NOT NULL DEFAULT '1',
  `router3` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routing`
--

/*!40000 ALTER TABLE `routing` DISABLE KEYS */;
INSERT INTO `routing` (`id`,`router1`,`router2`,`router3`) VALUES 
 (1,0,0,0);
/*!40000 ALTER TABLE `routing` ENABLE KEYS */;


--
-- Definition of table `split`
--

DROP TABLE IF EXISTS `split`;
CREATE TABLE `split` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p1` varchar(45) NOT NULL,
  `p2` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `split`
--

/*!40000 ALTER TABLE `split` DISABLE KEYS */;
INSERT INTO `split` (`id`,`p1`,`p2`) VALUES 
 (1,'0','0');
/*!40000 ALTER TABLE `split` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
